height = 22;
width = 22;
length = 0.7;
boreholeFragments = 200;
boreholeRadius = 0.1;
boreholePadding = 0.3;
filterInsertDistance = 2;
gratingThickness = 0.05;

module singleBorehole(x,y) {
    color([0,0,0]) {   
        translate([x,y,0])
        cylinder (
            h = length,
            r = boreholeRadius,
            center = true,
            $fn = boreholeFragments
        );
    }
}

module allBoreholes() {
    function calcXY() = (width / 2) - (boreholeRadius + boreholePadding);
   
    singleBorehole(-calcXY(),calcXY());
    singleBorehole(-calcXY(),-calcXY());
    singleBorehole(calcXY(),-calcXY());
    singleBorehole(calcXY(),calcXY());
}

module filterInsert() {
    color([1,0,0]) {
        translate([0,1,0])
        cube([
            width - filterInsertDistance,
            height,
            length - (boreholePadding * 1.5)],
            true);
    };
}

module grating() {
        color([0,1,0]) {
            //rotate(-45, [1, 0, 0])
            translate([0,0,0])
            cube([
                width - filterInsertDistance,
                gratingThickness,
                2
            ], true);
        };
    //}
}

difference () {
    color([1,1,1]) {
        cube([width, height, length], true);
    };
          
    allBoreholes();
    filterInsert();
    
    translate([0,-10,0])
    for ( i = [0:100] ) {
        translate([0,i/5,0])
        grating();
    }
   
}